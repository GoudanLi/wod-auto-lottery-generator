# WOD auto lottery generator

Generate lottery by creating new champions and level them up.

### Dependence
<pre>
Python 3.6+  
pip  
selenium
</pre>

### Deployment
1. install Python     
    * [Python 3.7+](https://www.python.org/downloads/release/python-372/)

2. Install dependencies manually:

    * [Pip](https://pip.pypa.io/en/stable/installing/)
    * [selenium](https://www.seleniumhq.org/projects/webdriver/)


    * ##### Or using following script (recommended, sudo needed)
        <pre>
        python get-pip.py
        pip install selenium
        </pre>  
3. running .py script


#### Thank you!

* Powered by ZDF
* Email: zdf@gwu.edu 

latest update: 2019-06-12
