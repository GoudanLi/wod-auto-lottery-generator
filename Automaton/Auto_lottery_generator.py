# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     Auto_lottery_ticket_generator
   Description :
   Author :       zdf's desktop
   date：          2019/5/9
-------------------------------------------------
   Change Activity:
                   2019/5/9:20:25
-------------------------------------------------
"""
import time

from selenium import webdriver
from selenium.webdriver.support.ui import Select


def create_name_by_timestamp():
    local_time = time.localtime(time.time())
    print(local_time)
    result = ""
    result += str(local_time.tm_mday)
    result += str(local_time.tm_hour)
    result += str(local_time.tm_min)
    result += str(local_time.tm_sec)
    print(result)
    return result


class Automaton:
    def __init__(self):
        # using chrome driver, alternative solution including Firefox, IE, Opera, etc.
        self.driver = webdriver.Chrome()

    def login(self, usr, pwd):
        """
        Login to your account
        :param usr: username
        :param pwd: password
        :return: None
        """
        self.driver.get("http://canto.world-of-dungeons.org")  # open WOD web page
        username = self.driver.find_element_by_id('USERNAME')
        username.send_keys(usr)  # fill the username
        password = self.driver.find_element_by_id('PASSWORT')
        password.send_keys(pwd)  # fill the password
        world = Select(self.driver.find_element_by_id('world'))
        world.select_by_value('CC')  # choose the world 'Canto'
        login_button = self.driver.find_element_by_xpath('//*[@id="WodLoginBox"]/table/tbody/tr[4]/td[2]/input')
        login_button.click()  # login in

    def goto_my_champion(self):
        champion = self.driver.find_element_by_xpath('/html/body/div[8]/div[2]/div/div/div/div/div/div[3]/a')
        champion.click()
        my_champion = self.driver.find_element_by_xpath('//*[@id="menu_my_heroes"]/a')
        my_champion.click()

    def goto_main_menu(self):
        main_menu = self.driver.find_element_by_xpath('//*[@id="menu_news"]/a')
        main_menu.click()

    def add_single_hero(self, champ_name):
        """
        Add a new champion
        :param champ_name: champion's name
        :return: None
        """
        self.goto_my_champion()
        create_new_champion = self.driver.find_element_by_xpath('//*[@id="main_content"]/div/form/p[2]/a')
        create_new_champion.click()
        name_input = self.driver.find_element_by_xpath('//*[@id="nameinput"]/tbody/tr[1]/td[2]/input')
        name_input.clear()
        name_input.send_keys("!" + champ_name + 'lottery')
        gender = self.driver.find_element_by_xpath('//*[@id="nameinput"]/tbody/tr[2]/td[2]/label[2]/input')
        gender.click()
        job = self.driver.find_element_by_xpath(
            '//*[@id="left_column"]/table/tbody/tr/td[1]/table/tbody/tr[1]/td/input')
        job.click()
        # wait = WebDriverWait(self.driver, 10)
        time.sleep(3)  # this part is native-Python and ugly, reconstruction in selenium is needed
        race = self.driver.find_element_by_xpath(
            '//*[@id="left_column"]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/input')
        # wait.until(expected_conditions.element_to_be_clickable(race))
        race.click()
        time.sleep(3)  # this part is Python-origin and ugly, refraction with selenium needed
        create = self.driver.find_element_by_xpath('//*[@id="left_column"]/input')
        create.click()

        self.goto_main_menu()

    def select_hero(self, num):
        """
        Select target champion by para num
        :param num: The serial of champions
        :return: None
        """
        self.goto_my_champion()
        xpath = '//*[@id="main_content"]/div/form/table/tbody/tr[' + str(num + 1) + ']/td[1]/input'
        hero_ratio = self.driver.find_element_by_xpath(xpath)
        hero_ratio.click()
        submit = self.driver.find_element_by_xpath('//*[@id="main_content"]/div/form/p[2]/input[1]')
        submit.click()

    def select_dungeon(self, num):
        """
        select and start correspond dungeons
        :param num: The serial of champions
        :return: None
        """
        self.select_hero(num)
        team = self.driver.find_element_by_xpath('/html/body/div[8]/div[2]/div/div/div/div/div/div[5]/a')
        team.click()
        dungeon = self.driver.find_element_by_xpath('//*[@id="menu_dungeon"]/a')
        dungeon.click()
        tavern = self.driver.find_element_by_name('visit[1]')
        tavern.click()
        self.goto_main_menu()

    def upgrade_attribute(self, num):
        """
        upgrade attributes for selected champions
        :param num: champions' number(eg:The first one = 1)
        :return:
        """
        self.select_hero(num)

        attribute = self.driver.find_element_by_xpath('//*[@id="menu_hero_attributes"]/a')
        attribute.click()

        add_str = self.driver.find_element_by_xpath('//*[@id="___wodToolTip_UniqueId__0"]')
        add_str.click()
        add_cons = self.driver.find_element_by_xpath('//*[@id="___wodToolTip_UniqueId__5"]')
        add_cons.click()
        add_dex = self.driver.find_element_by_xpath('//*[@id="___wodToolTip_UniqueId__6"]')
        add_dex.click()
        add_agi = self.driver.find_element_by_xpath('//*[@id="___wodToolTip_UniqueId__8"]')
        add_agi.click()

        self.goto_main_menu()

    def upgrade_skill(self, num):
        """
        upgrade skills for selected champions
        :param num: champions' number(eg:The first one = 1)
        :return:
        """
        self.select_hero(num)

        skill = self.driver.find_element_by_xpath('//*[@id="menu_hero_skills"]/a')
        skill.click()

        sword_using = self.driver.find_element_by_xpath('//*[@id="button_steigern_13"]')
        sword_using.click()
        time.sleep(1)
        submit = self.driver.find_element_by_xpath('//*[@id="skills_show"]/div[2]/p/input[1]')
        submit.click()

        self.goto_main_menu()

    def restore_select(self):
        self.goto_my_champion()
        for number in range(5, 13):
            xpath = '//*[@id="main_content"]/div/form/table/tbody/tr[' + str(number) + ']/td[4]/span/input'
            temp_champion = self.driver.find_element_by_xpath(xpath)
            temp_champion.click()
        first_champion = self.driver.find_element_by_xpath(
            '// *[ @ id = "main_content"] / div / form / table / tbody / tr[5] / td[1] / input')
        first_champion.click()
        self.goto_main_menu()
        pass

    def close(self):
        self.driver.close()


if __name__ == "__main__":
    pass
