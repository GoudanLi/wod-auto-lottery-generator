# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   Project Name:   Automaton
   File Name：     create_new_champions.py
   Description :
   Author :       workshop
   date：          2019/6/26 16:35
   
-------------------------------------------------
   Change Activity:
                   16:35
-------------------------------------------------
"""
from Auto_lottery_generator import *

if __name__ == "__main__":
    a = Automaton()
    a.login('Love, D.va', 'XXXXX')
    # replace X's with your password
    heroes_num = 3

    for i in range(1, heroes_num + 1):  # create new heroes by default
        champion_name = create_name_by_timestamp()
        a.add_single_hero(champion_name)
        a.upgrade_attribute(i)
        a.upgrade_skill(i)
        a.select_dungeon(i)

    a.restore_select()
    a.close()