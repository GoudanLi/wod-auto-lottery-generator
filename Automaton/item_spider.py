import re

import requests
from bs4 import BeautifulSoup as bs
from selenium.webdriver.support.select import Select


def duplicated_item_filter():
    regex = r'\([0-9]+x\)'
    with open('item_list.txt', 'r+') as f:
        item_list = []
        line = f.readline()
        while line:
            item_list.append(line)
            line = f.readline()
        item_set = set(item_list)
        item_list = list(item_set)
        res = []
        for v in item_list:
            res.append(re.sub(regex, ' ', v))
        with open('item_list_filtered.txt', 'r+') as ff:
            for v in res:
                ff.write(str(v))


def item_crawler():
    with open('item_list_filtered.txt', 'r+') as f:
        item_list = []
        line = f.readline()
        while line:
            item_list.append(line)
            line = f.readline()
        '''
        for item in item_list:
            regex = r'\[item:'
            item = re.sub(regex, '', item)
            item = item.replace(']', '')
            print(item)
            url = 'http://canto.world-of-dungeons.org/wod/spiel/hero/item.php?name=' + item
            r = requests.get(url=url)
            print(r.text)
        '''
        url = 'http://canto.world-of-dungeons.org/wod/spiel/hero/item.php?name=%2B1%E6%84%9F%E7%9F%A5%E4%B9%8B%E6%88%92'
        r = requests.get(url=url)
        r.encoding = 'utf-8'
        # print(r.text)
        soup = bs(r.text, 'lxml')
        # print(soup.prettify())
        name_tag = soup.find(class_='item_usable')
        name = name_tag.string
        desc_tag = name_tag.parent.next_sibling.next_sibling  # Attention! "\n" is count as a SIBLING!
        desc = desc_tag.string
        content_tag = soup.find(class_='content_table')
        career_requirement_tag = soup.find('span', text='职业限制')
        career_requirement = career_requirement_tag.parent.next_sibling.next_sibling.contents[1].string
        ethnicity_requirement_tag = soup.find('span', text='种族限定')
        ethnicity_requirement = ethnicity_requirement_tag.parent.next_sibling.next_sibling.contents[1].string
        equip_requirement_tag = soup.find('span', text='装备要求')
        equip_requirement = equip_requirement_tag.parent.next_sibling.next_sibling.contents[1].string
        durability_tag = soup.find('span', text='耐久度')
        durability = durability_tag.parent.next_sibling.next_sibling.contents[1].string
        value_tag = soup.find('span', text='价值')
        value = value_tag.parent.next_sibling.next_sibling.contents[1].string
        uniqueness_tag = soup.find('span', text='唯一性')
        uniqueness = uniqueness_tag.parent.next_sibling.next_sibling.contents[1].string
        charges_tag = soup.find('span', text='剩余使用次数')
        charges = charges_tag.parent.next_sibling.next_sibling.contents[1].string
        charges_per_dungeon_tag = soup.find('span', text='每地城可使用次数')
        charges_per_dungeon = charges_per_dungeon_tag.parent.next_sibling.next_sibling.contents[1].string
        charges_per_combat_tag = soup.find('span', text='每战斗可使用次数')
        charges_per_combat = charges_per_combat_tag.parent.next_sibling.next_sibling.contents[1].string
        effect_level_tag = soup.find('span', text='效果等级')
        effect_level = effect_level_tag.parent.next_sibling.next_sibling.contents[1].string
        matching_equipment_tag = soup.find('span', text='需配合何物使用')
        matching_equipment = matching_equipment_tag.parent.next_sibling.next_sibling.contents[1].string
        location_tag = soup.find('span', text='装备位置')
        location = location_tag.parent.next_sibling.next_sibling.contents[1].string
        type_bonus_tag = soup.find('span', text='物品类别')
        type_bonus = type_bonus_tag.parent.next_sibling.next_sibling.contents[1].string
        matching_skill_tag = soup.find('span', text='可以使用该物品的技能')
        matching_skill = matching_skill_tag.parent.next_sibling.next_sibling.contents[1].string
        designer_tag = soup.find('span', text='设计者')
        designer = designer_tag.parent.next_sibling.next_sibling.contents[1].string

        link_div = soup.find('div', id='link')
        # todo...


def select_vault(self):
    default = self.driver.current_window_handle
    champion = self.driver.find_element_by_xpath('/html/body/div[8]/div[2]/div/div/div/div/div/div[3]/a')
    champion.click()
    vault = self.driver.find_element_by_xpath('//*[@id="menu_hero_group_treasure"]/a')
    vault.click()
    career_list = ["冒险者", "吟游诗人", "圣武士", "学者", "射手", "杂耍艺人", "死灵师", "法师", "法师学徒", "漂泊客",
                   "炼金术士", "猎人", "祭司", "舞者", "萨满", "角斗士", "野蛮人", "骑士"]
    for i in range(len(career_list)):
        select = Select(self.driver.find_element_by_name('item_4hero_class'))
        select.select_by_visible_text(career_list[i])

        search = self.driver.find_element_by_xpath('//*[@id="main_content"]'
                                                   '/div/form/div[4]/table/tbody/tr/td[1]/span/a')
        search.click()
        bbcode_list = self.driver.find_element_by_xpath('//*[@id="main_content"]/div/form/div[5]/a[1]')
        bbcode_list.click()
        self.driver.switch_to.window(self.driver.window_handles[-1])
        item_list_obj = self.driver.find_element_by_xpath('/html/body/div[10]/div/div/form/table/tbody/tr/td/pre')
        item_list = item_list_obj.get_attribute("innerHTML")
        print(item_list)
        with open('item_list.txt', 'a+') as file:
            file.write(item_list)
        self.driver.close()
        self.driver.switch_to.window(default)
        vault = self.driver.find_element_by_xpath('//*[@id="menu_hero_group_treasure"]/a')
        vault.click()


if __name__ == "__main__":
    duplicated_item_filter()
    item_crawler()
